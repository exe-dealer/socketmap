function handleSocketMessage(e) {
  if (e.data instanceof ArrayBuffer) {
    var view = new DataView(e.data);
    var lat = view.getFloat64(0);
    var lng = view.getFloat64(8);
    var userId = view.getInt32(16);
    handleSomeoneMove(userId, lat, lng);
  } else {
    var message = JSON.parse(e.data);
    var eventName = message[0];
    var eventArgs = message.slice(1);
    var handler = serverEventHandlers[eventName];
    handler.apply(this, eventArgs);
  }
}

function handleSomeoneName(userId, userName) {
  setPopupContent(getMarkerByUserId(userId), userId, userName);
}

function handleSomeoneMove(userId, lat, lng) {
  var marker = getMarkerByUserId(userId);
  marker.setLatLng([lat, lng]);
  marker.addTo(othersLayer);
}

function handleSomeoneGone(userId) {
  var oldMarker = others[userId];
  if (oldMarker) {
    delete others[userId];
    oldMarker.remove();
  }
}

function getMarkerByUserId(userId) {
  return others[userId] || (others[userId] = createMarker(userId));
}

function createMarker(userId) {
  var marker = L.circleMarker(null, {
    color: 'red',
    fillOpacity: 0.8,
    radius: 5 /*px*/
  });
  setPopupContent(marker, userId);
  return marker;
}

function setPopupContent(marker, userId, userName) {
  marker.bindPopup(userName || ('#' + userId));
}

function handleSocketOpen() {
  document.body.classList.add('canjoin');
}

function handleSocketClose() {
  setTimeout(location.reload.bind(location), 5000);
}

function handleInitialPosition(position) {
  handleMyPositionChanged(position);
  me.addTo(map);
  me.openPopup();
  map.panTo(me.getLatLng());

  // navigator.geolocation.watchPosition(
  //   handleMyPositionChanged,
  //   handleGeolocationError,
  //   geolocationOptions
  // );
}

function handleMeDrag(e) {
  var pos = me.getLatLng();
  sendMyLatLng(pos.lat, pos.lng);
}

function handleMyPositionChanged(position) {
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;
  me.setLatLng([lat, lng]);
  sendMyLatLng(lat, lng);
}

function sendMyLatLng(lat, lng) {
  var buffer = new ArrayBuffer(16);
  var view = new DataView(buffer);
  view.setFloat64(0, lat);
  view.setFloat64(8, lng);
  sock.send(buffer);
}

function handleGeolocationError(error) {
  switch(error.code) {
  case error.PERMISSION_DENIED:
    alert('User denied the request for Geolocation.');
    break;
  case error.POSITION_UNAVAILABLE:
    alert('Location information is unavailable.');
    break;
  case error.TIMEOUT:
    alert('The request to get user location timed out.');
    break;
  case error.UNKNOWN_ERROR:
    alert('An unknown error occurred.');
    break;
  }
}

function handleJoinClick() {
  navigator.geolocation.getCurrentPosition(
    handleInitialPosition,
    handleGeolocationError,
    geolocationOptions
  );

  document.body.classList.remove('canjoin');
  /*
  handleInitialPosition({
    coords: {
      latitude: leninSatpaev.lat
      longitude: leninSatpaev.lng
    }
  });
  */
}


var serverEventHandlers = {
  'name': handleSomeoneName,
  'gone': handleSomeoneGone
};



var myNameEl = document.createElement('input');
myNameEl.placeholder = 'Мое имя';
myNameEl.id = 'myname';
myNameEl.onkeyup = function (e) {
  if (e.keyCode === 13) {
    me.closePopup();
  }
};


var popup = L.popup();
popup.setContent(myNameEl);

var me = L.marker(null, { draggable: true });
me.bindPopup(popup);
me.on('drag', handleMeDrag);
me.on('popupopen', myNameEl.focus.bind(myNameEl));
me.on('popupclose', function () {
  sock.send(JSON.stringify(['iam', myNameEl.value]));
});


var othersLayer = L.layerGroup();

var others = {};


var wsProtocol = location.protocol == 'https:' ? 'wss:' : 'ws:'
var sock = new WebSocket(wsProtocol + '//' + location.host);
sock.binaryType = 'arraybuffer';
sock.onmessage = handleSocketMessage;
sock.onopen = handleSocketOpen;
sock.onclose = handleSocketClose;
setInterval(function () {
  sock.send('["alive"]');
}, 10000)


var geolocationOptions = {
  enableHighAccuracy: true,
  maximumAge: 5000 // 5 sec.
};


document.getElementById('join').onclick = handleJoinClick;


var moscow = [[55.916121123547924, 37.32879638671875],
        [55.55970923563195,37.95501708984375]];

var map = L.map('map', {
  // preferCanvas: true
});
map.addLayer(othersLayer);
map.fitBounds(moscow);

L.tileLayer(location.protocol + '//tile{s}.maps.2gis.com/tiles?x={x}&y={y}&z={z}', {
  subdomains: '0123',
  maxZoom: 18
}).addTo(map);
