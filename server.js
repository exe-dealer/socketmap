var http = require('http')
var ws = require('ws');
var serveStaticMod = require('serve-static');
var finalhandler = require('finalhandler');

var httpServer = http.createServer();
var websocketServer = new ws.Server({ server: httpServer });

var lastUserId = 0;

websocketServer.on('connection', function (currentClient) {

  function handleIncomingMessage(message) {
    if (message instanceof Buffer) {
      handleCoords.apply(null, unpackCoordsMessage(message));
    } else {
      var jsonMessage;
      try {
        jsonMessage = JSON.parse(message);
      }
      catch (e) {
        return currentClient.send(JSON.stringify(['err', String(e)]));
      }
      var messageTag = jsonMessage[0];
      if (messageTag === 'iam') handleIAm.apply(null, jsonMessage.slice(1));
    }
  }

  function handleCoords(lat, lng) {
    currentClient.lat = lat;
    currentClient.lng = lng;

    broadcast(createCoordsMessage(
      currentClient.lat,
      currentClient.lng,
      currentClient.userId
    ));
  }

  function handleIAm(name) {
    currentClient.name = (name === null ? null : name.toString());

    broadcast(JSON.stringify([
      'name',
      currentClient.userId,
      currentClient.name
    ]));
  }

  function handleClose() {
    broadcast(JSON.stringify([
      'gone',
      currentClient.userId
    ]));
  }

  function broadcast(message) {
    websocketServer.clients.forEach(function (client) {
      if (client !== currentClient) {
        client.send(message);
      }
    });
  }

  currentClient.userId = ++lastUserId;
  currentClient.on('message', handleIncomingMessage);
  currentClient.on('close', handleClose);

  websocketServer.clients.forEach(function (client) {
    if (client !== currentClient) {

      if (typeof client.lat == 'number' &&
          !isNaN(client.lat) &&
          typeof client.lng == 'number' &&
          !isNaN(client.lng))
      {
        currentClient.send(createCoordsMessage(
          client.lat,
          client.lng,
          client.userId
        ));
      }

      if (client.name) {
        currentClient.send(JSON.stringify([
          'name',
          client.userId,
          client.name
        ]));
      }
    }
  });

});

function createCoordsMessage(lat, lng, userId) {
  var message = new Buffer(20);
  message.writeDoubleBE(lat, 0);
  message.writeDoubleBE(lng, 8);
  message.writeInt32BE(userId, 16);
  return message;
}

function unpackCoordsMessage(message) {
  return [message.readDoubleBE(0), message.readDoubleBE(8)]
}

var serveStatic = serveStaticMod('ui');

httpServer.on('request', function (req, res) {
  var done = finalhandler(req, res)
  serveStatic(req, res, done)
});

httpServer.listen(process.env.PORT || 8000);
